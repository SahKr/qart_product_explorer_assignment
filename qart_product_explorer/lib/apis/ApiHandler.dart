import 'package:http/http.dart' as http;
import 'package:qart_product_explorer/mappers/ApiCollectionMapper.dart';
import 'package:qart_product_explorer/mappers/ProductDataMapper.dart';
import 'package:qart_product_explorer/models/ApiCollectionModel.dart';
import 'package:qart_product_explorer/models/ProductModel.dart';
import 'package:qart_product_explorer/values/Constant.dart';

class ApiHandler{

  //method to call api_collection api
  Future<ApiCollectionModel> hitApiCollection() async{
    final response = await http.get(API_COLLECTION_URL);

    try{
      if(response.statusCode == 200){
        return ApiCollectionMapper().apiMapper(response.body);
      }else{
        return null;
      }
    }catch(e){
      return null;
    }
  }
  //method to call product api
  Future<ProductDataModel> hitProductApi(String url, String method) async{
    var response;
    try{
      if(method == 'GET')
        response = await http.get(url);
      else
        response = await http.post(url);

      if(response.statusCode == 200){
        return ProductDataMapper().apiMapper(response.body);
      }else{
        return null;
      }
    }catch(e){
      return null;
    }
  }

}