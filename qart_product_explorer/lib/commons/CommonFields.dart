import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/services.dart';

//common widget for fadeIn network image with placeholder
Widget ImageViewNetworkPlaceHolder({
  EdgeInsetsGeometry margin = const EdgeInsets.all(0),
  EdgeInsetsGeometry padding = const EdgeInsets.all(0),
  String placeHolder = '',
  String imagePath = '',
  double height = null,
  double width = null,
}) {
  return Container(
    height: height,
    width: width,
    margin: margin,
    child: Padding(
      padding: padding,
      child:
      FadeInImage.assetNetwork(placeholder: placeHolder, image: imagePath),
    ),
  );
}

//custom text widget
Widget TextView(
    {EdgeInsetsGeometry margin = const EdgeInsets.all(0),
      EdgeInsetsGeometry padding = const EdgeInsets.all(0),
      String title = '',
      double fontSize = 13,
      Color fontColor = Colors.black54,
      FontStyle fontStyle = FontStyle.normal,
      FontWeight fontWeight = FontWeight.normal,
      TextAlign textAlign = TextAlign.left,
      Color containerBg = Colors.transparent,
      double width = null}) {
  return Container(
    margin: margin,
    width: width,
    color: containerBg,
    child: Padding(
      padding: padding,
      child: title != null && title.isNotEmpty
          ? Text(
        title,
        overflow: TextOverflow.ellipsis,
        maxLines: 100,
        style: TextStyle(
          fontSize: fontSize,
          fontFamily: 'VarelaRound',
          color: fontColor,
          fontStyle: fontStyle,
          fontWeight: fontWeight,
        ),
        textAlign: textAlign,
      )
          : Container(),
    ),
  );
}

//custom textfield with border
Widget EdiTextBox({
  EdgeInsetsGeometry margin = const EdgeInsets.all(0),
  EdgeInsetsGeometry padding = const EdgeInsets.all(0),
  Color borderColor = Colors.grey,
  TextEditingController controller,
  Color cursorColor = Colors.red,
  TextInputType inputType = TextInputType.text,
  TextInputAction inputTypeAction = TextInputAction.done,
  bool autoFocus = false,
  int maxlength = 1000,
  int maxline = 1,
  List<TextInputFormatter> formatters,
  FocusNode focus,
  String label = '',
  Widget prefix = null,
  String hint = '',
  String errorText=null,
  TextCapitalization textCapitalize = TextCapitalization.sentences,
  ValueChanged<String> onSubmitted,
  ValueChanged<String> onChanged,
  bool interactiveSelection  = true,
  bool enabled = true,
  Color disabledColor = Colors.grey,
  Color fontColor = Colors.black54,
  //onTab method for car price check
  void onTab(),
  TextAlign align = TextAlign.start,
}) {
  return new Container(
    margin: margin,
    child: new Theme(
      data: new ThemeData(
        primaryColor: borderColor,
        disabledColor: disabledColor,
      ),
      child: new TextField(
        controller: controller != null ? controller : TextEditingController(),
        keyboardType: inputType,
        enableInteractiveSelection :interactiveSelection,
        textInputAction: inputTypeAction,
        autofocus: autoFocus,
        maxLength: maxlength,
        maxLines: maxline,
        textAlign: align,
        enabled: enabled,
        style: TextStyle(
          color: fontColor,
          fontSize: 15,
          fontFamily: 'VarelaRound',
        ),
        focusNode: focus != null ? focus : new FocusNode(),
        inputFormatters: formatters != null ? formatters : [],
        decoration: new InputDecoration(
            border: new OutlineInputBorder(),
            disabledBorder: new OutlineInputBorder(borderSide: BorderSide(width: 1.5, color: disabledColor)),
            hintText: hint,
            errorText:errorText,
            prefixIcon: prefix,
            labelText: label,
            labelStyle: TextStyle(fontSize: 15),
            counterText: ''),
        cursorColor: cursorColor,
        textCapitalization: textCapitalize,
        onSubmitted: onSubmitted,
        onChanged: onChanged,
        onTap: onTab,
      ),
    ),
  );
}

//common dialog box for failure/success msg
showDialogMsg(BuildContext context, String msg) async {
  return showDialog(
    context: context,
    barrierDismissible: true,
    builder: (BuildContext context) {
      return AlertDialog(
        content: Text(
            msg,
            ),
        actions: [
          FlatButton(
            onPressed: (){
              Navigator.of(context).pop();
            },
            child: TextView(
              title: 'OK',
              fontColor: Colors.blue,
            ),
          )
        ],
      );
    },
  );
}

Widget iconContainer(
    IconData icon, EdgeInsets margin, VoidCallback onTap, double size, Color color) {
  return Container(
    margin: margin,
    decoration:
    BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(10))),
    child: InkWell(
        onTap: onTap,
        splashColor: Colors.transparent,
        highlightColor: Colors.transparent,
        child: Icon(
          icon,
          color: color,
          size: size,
        )),
  );
}