import 'dart:io';

import 'package:path_provider/path_provider.dart';
import 'package:qart_product_explorer/values/Constant.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

class DatabaseHelper{

  static final _dbName = 'QArt_Products.db';
  static final _dbVersion = 1;
  //static final _tableName = 'productData';

  //making it a singleton class
  DatabaseHelper._privateConstructor();
  static final DatabaseHelper instance = DatabaseHelper._privateConstructor();

  static Database _database;

  Future<Database> get database async{
    if(_database != null) return _database;

    _database = await _initDB();
    return _database;
  }

  //method to initialize database
  _initDB() async{
    Directory directory = await getApplicationDocumentsDirectory();
    String path = join(directory.path, _dbName);
    return await openDatabase(path, version: _dbVersion, onCreate: _onCreate);

  }

  //method to create new table in database
  Future _onCreate(Database db, int version) {
     db.execute(
      '''
      CREATE TABLE $STORE_PRODUCTS(
      $OPTION TEXT PRIMARY KEY,
      $SEASON TEXT,
      $BRAND TEXT,
      $MOOD TEXT NOT NULL,
      $GENDER TEXT,
      $THEME TEXT,
      $CATEGORY TEXT,
      $NAME TEXT,
      $COLOR TEXT,
      $QRCODE TEXT,
      $MRP TEXT,
      $SUBCATEGORY TEXT,
      $COLLECTION TEXT,
      $DESCRIPTION TEXT,
      $FABRIC TEXT,
      $EANS TEXT,
      $FINISH TEXT,
      $FIT TEXT,
      $TECHNOLOGY TEXT,
      $LIKEABILTY TEXT,
      $OFF_MONTHS TEXT,
      $TECH_IMAGE_URL TEXT,
      $IMAGEURL TEXT)
      '''
    );
     db.execute(
         '''
      CREATE TABLE $CART_PRODUCTS(
      $OPTION TEXT PRIMARY KEY,
      $SEASON TEXT,
      $BRAND TEXT,
      $MOOD TEXT NOT NULL,
      $GENDER TEXT,
      $THEME TEXT,
      $CATEGORY TEXT,
      $NAME TEXT,
      $COLOR TEXT,
      $QRCODE TEXT,
      $MRP TEXT,
      $SUBCATEGORY TEXT,
      $COLLECTION TEXT,
      $DESCRIPTION TEXT,
      $FABRIC TEXT,
      $EANS TEXT,
      $FINISH TEXT,
      $FIT TEXT,
      $OFF_MONTHS TEXT,
      $TECHNOLOGY TEXT,
      $LIKEABILTY TEXT,
      $ADDED_QTY INTEGER,
      $TECH_IMAGE_URL TEXT,
      $IMAGEURL TEXT)
      '''
     );

  }

  //method to insert data into table
  Future<int> insert(Map<String, dynamic> row, String tableName) async{
    Database db = await instance.database;
    return await db.insert(tableName, row);
  }

  //method to select all row in table
  Future<List<Map<String, dynamic>>> queryAll(String tableName) async{
    Database db = await instance.database;
    return await db.query(tableName,);
  }

  //method to update rows in table
  Future<int> update(Map<String, dynamic> row, String tableName, String option) async{
    Database db = await instance.database;
    return await db.update(tableName, row, where: '$OPTION = ?', whereArgs: [option]);
  }

  //method to delete all rows in the table
  Future<int> delete(String tableName) async{
    Database db = await instance.database;
    return await db.delete(tableName);
  }

  //method to delete specific rows in the table
  Future<int> deleteSpecific(String tableName, String option) async{
    Database db = await instance.database;
    return await db.delete(tableName, where: '$OPTION = ?', whereArgs: [option]);
  }

  //method to execute raw query
  Future<List<Map<String, dynamic>>> query(String sql) async{
    Database db = await instance.database;
    return await db.rawQuery(sql);
  }

 /* Future<int> checkSize() async{
    Database db = await instance.database;
    return  Sqflite.firstIntValue(await db.rawQuery('SELECT COUNT(*) FROM $_tableName'));
  }*/

}