import 'package:flutter/material.dart';
import 'package:qart_product_explorer/screens/SplashScreen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'QArt Explorer',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          // Define the default brightness and colors.
//          brightness: Brightness.dark,
//          primaryColor: Colors.lightBlue[800],
//          accentColor: Colors.cyan[600],

          // Define the default font family.
          fontFamily: 'VarelaRound',
          bottomSheetTheme: BottomSheetThemeData(
            backgroundColor: Colors.black.withOpacity(0),
          )

          // Define the default TextTheme. Use this to specify the default
          // text styling for headlines, titles, bodies of text, and more.
//          textTheme: TextTheme(
//            headline1: TextStyle(fontSize: 72.0, fontWeight: FontWeight.bold),
//            headline6: TextStyle(fontSize: 36.0, fontStyle: FontStyle.italic),
//            bodyText2: TextStyle(fontSize: 14.0, fontFamily: 'Hind'),
//          ),
        ),
        home: SplashScreen(),);
  }
}


