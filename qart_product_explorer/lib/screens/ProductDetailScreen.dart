import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:qart_product_explorer/commons/CommonFields.dart';
import 'package:qart_product_explorer/database/DatabaseHelper.dart';
import 'package:qart_product_explorer/models/ProductModel.dart';
import 'package:qart_product_explorer/screens/CartScreen.dart';
import 'package:qart_product_explorer/values/Constant.dart';

class ProductDetailScreen extends StatefulWidget {
  ProductModel product = new ProductModel();
  ProductDetailScreen(this.product);

  @override
  _ProductDetailScreenState createState() => _ProductDetailScreenState();
}

class _ProductDetailScreenState extends State<ProductDetailScreen> {

  //variable to count number of product selected
  int qtySelected = 0;

  //bool list to keep track which product is selected as all the 30 product are same
  List<bool> productSelectionFlags = new List();

  //List to store selected products
  List<ProductModel> selectedProducts = new List();

  //controller for product detail fields
  TextEditingController _brandCategoryController = new TextEditingController();
  TextEditingController _genderNameController = new TextEditingController();
  TextEditingController _fitThemeController = new TextEditingController();
  TextEditingController _offMonthsController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _appBar(),
      body: Container(
        color: Colors.red.withOpacity(0.1),
        padding: EdgeInsets.only(top: 12, left: 12, right: 12),
        child: Stack(
          children: [
            Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    TextView(
                        title: '30 Options (Tap to book)',
                        fontColor: Colors.black87,
                        fontSize: 17),
                    TextView(
                        title: 'Overall Qty:$qtySelected',
                        fontColor: Colors.black87,
                        fontSize: 17),
                  ],
                ),
                Padding(
                  padding: EdgeInsets.all(6),
                ),
                Expanded(
                  child: NotificationListener<OverscrollIndicatorNotification>(
                    onNotification: (overscroll) {
                      overscroll.disallowGlow();
                      return true;
                    },
                    child: SingleChildScrollView(
                      child: Container(
                        margin: EdgeInsets.only(bottom: 32),
                        child: Wrap(
                            alignment: WrapAlignment.start,
                            runAlignment: WrapAlignment.start,
                            crossAxisAlignment: WrapCrossAlignment.start,
                            spacing: 14,
                            runSpacing: 4,

//              crossAxisAlignment: WrapCrossAlignment.start,
                            direction: Axis.horizontal,
                            children: _listProductCard()),
                      ),
                    ),
                  ),
                )
              ],
            ),
            _productDetailViewButton()
          ],
        ),
      ),
    );
  }

  //widget for appbar
  Widget _appBar() {
    return AppBar(
      elevation: 0,
      titleSpacing: 0,
      backgroundColor: Colors.white,
      automaticallyImplyLeading: true,
      iconTheme: IconThemeData(
        color: Colors.black87,
      ),
      title: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Image.asset(
            'assets/images/qart_logo.png',
            height: 38,
            width: 38,
            fit: BoxFit.scaleDown,
          ),
          TextView(title: 'art', fontColor: Color.fromRGBO(218, 0, 0, 1), fontSize: 21),
          TextView(title: widget.product.qrcode, fontColor: Colors.black87, fontSize: 13, margin: EdgeInsets.only(left: 8))
        ],
      ),
      actions: [
        iconContainer(Icons.shopping_cart, EdgeInsets.only(right: 8), () {
          print('cart button pressed!');
          Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext ctx){
            return CartScreen();
          }));

        }, 26, Color.fromRGBO(218, 0, 0, 1)),
        Container(
          margin: EdgeInsets.only(right: 12, top: 8, bottom: 8),
          child: MaterialButton(
            elevation: qtySelected>0?5:2,
            color: qtySelected>0?Color.fromRGBO(218, 0, 0, 1):Colors.black45,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20.0),
              //side: BorderSide(color: Colors.red)
            ),
            onPressed: () {
              if(qtySelected>0)
                _addToCart();
            },
            child: TextView(
              title: 'Add To Cart',
              fontSize: 12,
              fontColor: Colors.white,
            ),
          ),
        ),
      ],
    );
  }

  //list of widget to draw 30 product cards
  List<Widget> _listProductCard() {
    List<ProductModel> cards = new List();
    for (int i = 0; i < 30; i++) {
      ProductModel newModel = new ProductModel();
      newModel.season = widget.product.season;
      newModel.description = widget.product.description;
      newModel.fabric = widget.product.fabric;
      newModel.finish = widget.product.finish;
      newModel.likeability = widget.product.likeability;
      newModel.mood = widget.product.mood;
      newModel.theme = widget.product.theme;
      newModel.color = widget.product.color;
      newModel.option = widget.product.option;
      newModel.qrcode = widget.product.qrcode;
      newModel.brand = widget.product.brand;
      newModel.category = widget.product.category;
      newModel.collection = widget.product.collection;
      newModel.gender = widget.product.gender;
      newModel.name = widget.product.name;
      newModel.subCategory = widget.product.subCategory;
      newModel.offerMonths = widget.product.offerMonths;
      newModel.mrp = widget.product.mrp;
      newModel.fit = widget.product.fit;
      newModel.tech = widget.product.tech;
      newModel.eans = widget.product.eans;
      newModel.imageUrl = widget.product.imageUrl;
      newModel.techImageUrl = widget.product.techImageUrl;
      newModel.index = i;
      cards.add(newModel);
      productSelectionFlags.add(false);
    }

    //print(cards[2].index);
    return cards.map((e) {
      return _productViewCard(
        e,
        () {
          setState(() {
            productSelectionFlags[e.index] = !productSelectionFlags[e.index];
            if (productSelectionFlags[e.index]){
              qtySelected++;
              selectedProducts.add(e);
            }
            else{
              qtySelected--;
              selectedProducts.removeWhere((element) => element.index == e.index);
            }
          });

          print('Product ${e.index}  tapped!');
        },
      );
    }).toList();
  }

  //button to display product details
  Widget _productDetailViewButton() {
    return Align(
      alignment: Alignment.bottomCenter,
      child: Card(
        margin: EdgeInsets.only(bottom: 6),
        elevation: 5,
        child: InkWell(
          onTap: (){
            _showBottomSheet(context);
          },
          child: Container(
            decoration: BoxDecoration(
                color: Color.fromRGBO(218, 0, 0, 1),
              borderRadius: BorderRadius.all(Radius.circular(4))
            ),
            width: MediaQuery.of(context).size.width,
            padding: EdgeInsets.all(6),
            child: TextView(
                title:'Product Details',
                fontColor: Colors.white,
                fontSize: 15,
                textAlign: TextAlign.center
            ),
          ),
        ),
      ),
    );
  }

  //method to show bottom sheet when product details button is pressed
  void _showBottomSheet(context){
    showModalBottomSheet(context: context, builder: (BuildContext ctx){
      return Container(
        height: MediaQuery.of(context).size.height * 0.45,
//        margin: EdgeInsets.only(
//          top: MediaQuery.of(context).size.height * 0.43,
//        ),
        child: ClipRRect(
          borderRadius: BorderRadius.all(Radius.circular(5)),
          child: BackdropFilter(
            filter: ImageFilter.blur(sigmaX: 5, sigmaY: 5),
            child: Column(
              children: [
                Container(
                  height: MediaQuery.of(context).size.height*0.43,
                  margin: EdgeInsets.only(left: 12, right: 12, top: 8),
                  child: ListView(
                    shrinkWrap: true,
                    children: [
                      Container(
                        margin: EdgeInsets.only(top: 2),
                        padding: EdgeInsets.only(
                            top: 10, left: 12, right: 12, bottom: 10),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.all(Radius.circular(5))),
                        child: Column(
                          children: [
                            EdiTextBox(
                                label: 'Brand - Category - Collection',
                                borderColor: Colors.red,
                                enabled: false,
                                disabledColor: Colors.red,
                                fontColor: Colors.black,
                                controller: _brandCategoryController,
                                margin: EdgeInsets.only(bottom: 8, top: 2)),
                            EdiTextBox(
                                label: 'Gender - Name - Subcategory',
                                borderColor: Colors.red,
                                disabledColor: Colors.red,
                                enabled: false,
                                fontColor: Colors.black,
                                controller: _genderNameController,
                                margin: EdgeInsets.only(bottom: 8)),
                            EdiTextBox(
                                label: 'Fit - Theme - Finish',
                                borderColor: Colors.red,
                                disabledColor: Colors.red,
                                enabled: false,
                                fontColor: Colors.black,
                                controller: _fitThemeController,
                                margin: EdgeInsets.only(bottom: 8)),
                            EdiTextBox(
                              label: 'Off_Months - Mood',
                              borderColor: Colors.red,
                              fontColor: Colors.black,
                              disabledColor: Colors.red,
                              controller: _offMonthsController,
                              enabled: false,
                              // margin: EdgeInsets.only(bottom: 8)
                            ),
                          ],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 8),
                        padding: EdgeInsets.only(
                            top: 10, left: 12, right: 12, bottom: 10),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.all(Radius.circular(5))),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            TextView(
                                title: 'Description',
                                fontSize: 16,
                                fontColor: Colors.red,
                                margin: EdgeInsets.only(bottom: 8)
                            ),
                            TextView(
                              title: '${widget.product.description}',
                              fontSize: 14,
                              fontColor: Colors.black,
                            )
                          ],
                        ),
                      ),
                      /*Container(
                        margin: EdgeInsets.only(top: 8, bottom: 8),
                        padding: EdgeInsets.only(
                            top: 10, left: 12, right: 12, bottom: 10),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.all(Radius.circular(5))),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Column(
                                  children: [
                                    TextView(
                                        title: 'Category_Width',
                                        fontSize: 15,
                                        fontColor: Colors.red,
                                        margin: EdgeInsets.only(bottom: 8)
                                    ),
                                    TextView(
                                      title: '3245',
                                      fontSize: 14,
                                      fontColor: Colors.black,
                                      //margin: EdgeInsets.only(bottom: 8)
                                    ),
                                  ],
                                ),
                                Column(
                                  children: [
                                    TextView(
                                        title: 'Category_Total',
                                        fontSize: 15,
                                        fontColor: Colors.red,
                                        margin: EdgeInsets.only(bottom: 8)
                                    ),
                                    TextView(
                                      title: '43546',
                                      fontSize: 14,
                                      fontColor: Colors.black,
                                      //margin: EdgeInsets.only(bottom: 8)
                                    ),
                                  ],
                                ),
                                Column(
                                  children: [
                                    TextView(
                                        title: 'Technology',
                                        fontSize: 15,
                                        fontColor: Colors.red,
                                        margin: EdgeInsets.only(bottom: 8)
                                    ),
                                    TextView(
                                      title: '${widget.product.tech} ',
                                      fontSize: 14,
                                      fontColor: Colors.red,
                                      //margin: EdgeInsets.only(bottom: 8)
                                    ),
                                  ],
                                ),
                              ],
                            ),

                          ],
                        ),
                      ),*/
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      );
    });
  }

  //widget to design product card
  Widget _productViewCard(ProductModel model, VoidCallback onTap) {
    return AnimatedContainer(
      duration: Duration(milliseconds: 250),
      height: 230,
      width: 160,
      margin: EdgeInsets.only(bottom: 8),
      decoration: BoxDecoration(
          color: Colors.white,
          border: Border.all(
              color: productSelectionFlags[model.index]
                  ? Colors.red
                  : Colors.transparent,
              width: productSelectionFlags[model.index] ? 2 : 0),
          borderRadius: BorderRadius.all(Radius.circular(15)),
          boxShadow: [
            BoxShadow(
                color: Colors.black12, offset: Offset(0.0, 1.0), blurRadius: 6)
          ]),
      child: Stack(
        children: [
          InkWell(
            onTap: onTap,
            child: Stack(
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Expanded(
                        child: ClipRRect(
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(13.5),
                                topRight: Radius.circular(13.5)),
                            child: FadeInImage.assetNetwork(
                              placeholder: 'assets/images/qart_logo.png',
                              image: model.imageUrl,
                              fit: BoxFit.fitHeight,
                            ))),
                    Container(
                      padding: EdgeInsets.only(left: 5, right: 5),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.only(
                              bottomLeft: Radius.circular(15),
                              bottomRight: Radius.circular(15))),
                      height: 25,
                      child: _getLikeabiltyStars(model)
                    )
                  ],
                ),
                Container(
                  padding: EdgeInsets.all(6),
                  height: 49,
                  width: 175,
                  margin: EdgeInsets.only(bottom: 25, top: 156),
                  color: Colors.black38,
                  child: TextView(
                      title: model.option + '\n' + model.color + '\n₹' + model.mrp,
                      fontColor: Colors.white,
                      fontSize: 10,
                      textAlign: TextAlign.center),
                ),
              ],
            ),
          ),
          Container(
            height: 35,
            width: 35,
            margin: EdgeInsets.only(top: 97.5),
            decoration: BoxDecoration(
                color: Colors.white70,
                boxShadow: [BoxShadow(color: Colors.grey, blurRadius: 6, offset: Offset(0.0, 1.0))],
                borderRadius: BorderRadius.only(topRight: Radius.circular(17.5), bottomRight: Radius.circular(17.5),)
            ),
            child: Icon(
              Icons.style, color: Color.fromRGBO(218, 0, 0, 1),
            ),
          ),
          Container(
            height: 35,
            width: 35,
            margin: EdgeInsets.only(top: 97.5, left: 125),
            decoration: BoxDecoration(
                color: Colors.white70,
                boxShadow: [BoxShadow(color: Colors.grey, blurRadius: 6, offset: Offset(0.0, 1.0))],
                borderRadius: BorderRadius.only(topLeft: Radius.circular(17.5), bottomLeft: Radius.circular(17.5),)
            ),
            child: Icon(
              Icons.turned_in_not, color: Color.fromRGBO(218, 0, 0, 1),
            ),
          )
        ],
      ),
    );
  }

  //widget to draw rating stars
  Widget _getLikeabiltyStars(ProductModel model){
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Row(
          children: _getStars(model),
        ),
        TextView(
            title: '(${_getLikebility(model)})', fontColor: (double.parse(model.likeability.trim())/2 > 0)?Colors.red:Colors.black54, fontSize: 14)
      ],
    );
  }

  //list of widget to draw enable and disabled stars according to rating
  List<Widget> _getStars(ProductModel model){
    double rating = double.parse(model.likeability.trim())/2;
    List<Widget> stars = new List();
    for(int i=1; i<= rating; i++){
      stars.add(_starIcon(Color.fromRGBO(218, 0, 0, 1), false));
    }

    for(int i=1; i<= 5- rating; i++){
      stars.add(_starIcon(Colors.black54, true));
    }

    return stars;
  }

  //method to get rating in number
  String _getLikebility(ProductModel model){
    double rating = double.parse(model.likeability.trim())/2;
    return rating.toStringAsFixed(1);
  }

  //widget to draw individual stars
  Widget _starIcon(Color color, bool emptyStar){
    return Icon(
      emptyStar?Icons.star_border:Icons.star,
      color: color,
      size: 17,
    );
  }

  //method called when add to cart button is presses also it navigate to cart screen
  _addToCart() async{

    for(int i=0; i< selectedProducts.length; i++){
      int existedData =0;
      existedData = await DatabaseHelper.instance.update({OPTION : selectedProducts[i].option+selectedProducts[i].index.toString()},
          CART_PRODUCTS, selectedProducts[i].option+selectedProducts[i].index.toString());
    if(existedData == null || existedData==0){
      await DatabaseHelper.instance.insert({
        SEASON: selectedProducts[i].season,
        BRAND: selectedProducts[i].brand,
        MOOD: selectedProducts[i].mood,
        GENDER: selectedProducts[i].gender,
        THEME: selectedProducts[i].theme,
        CATEGORY: selectedProducts[i].category,
        NAME: selectedProducts[i].name,
        COLOR: selectedProducts[i].color,
        OPTION: selectedProducts[i].option+selectedProducts[i].index.toString(),
        MRP: selectedProducts[i].mrp,
        SUBCATEGORY: selectedProducts[i].subCategory,
        COLLECTION: selectedProducts[i].collection,
        DESCRIPTION: selectedProducts[i].description,
        QRCODE: selectedProducts[i].qrcode,
        FABRIC: selectedProducts[i].fabric,
        EANS: selectedProducts[i].eans,
        FINISH: selectedProducts[i].finish,
        IMAGEURL: selectedProducts[i].imageUrl,
        FIT:selectedProducts[i].fit,
        TECHNOLOGY:selectedProducts[i].tech,
        LIKEABILTY:selectedProducts[i].likeability,
        TECH_IMAGE_URL:selectedProducts[i].techImageUrl,
        ADDED_QTY: 1
      }, CART_PRODUCTS);
    }
    else
      continue;

    }

    print("Added to cart!");
    Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext ctx){
      return CartScreen();
    }));
  }

  @override
  void initState() {
    super.initState();

    //setting the values of product detail fields during initialisation
    setState(() {
      _brandCategoryController.text = widget.product.brand +
          '-' +
          widget.product.category +
          '-' +
          widget.product.collection;
      _genderNameController.text = widget.product.gender +
          '-' +
          widget.product.name +
          '-' +
          widget.product.subCategory;
      _fitThemeController.text = widget.product.fit +
          '-' +
          widget.product.theme +
          '-' +
          widget.product.finish;
      _offMonthsController.text = widget.product.offerMonths.toString()+'-'+widget.product.mood;
    });
  }
}
