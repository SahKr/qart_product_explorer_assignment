import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:qart_product_explorer/commons/CommonFields.dart';
import 'package:qart_product_explorer/database/DatabaseHelper.dart';
import 'package:qart_product_explorer/models/ProductModel.dart';
import 'package:qart_product_explorer/values/Constant.dart';

class CartScreen extends StatefulWidget {

  @override
  _CartScreenState createState() => _CartScreenState();
}

class _CartScreenState extends State<CartScreen> {

  //list to store product added to carts
  List<ProductModel> addedProducts = new List();

  //variable to keep track of number of items
  int _totalQty = 0;

  //variable to keep track of subtotal price
  double _subtotal = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _appBar(),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            constraints: BoxConstraints(maxHeight: MediaQuery.of(context).size.height-203),
            padding: EdgeInsets.all(6),
            //color: Colors.black12,
//            height: MediaQuery.of(context).size.height-170,
            child: _productCardView()
          ),
          Expanded(child: Container(),),
          _buyButtonCard()
        ],
      ),
    );
  }

  //widget to draw appbar
  Widget _appBar() {
    return AppBar(
      elevation: 0,
      titleSpacing: 0,
      backgroundColor: Colors.white,
      automaticallyImplyLeading: true,
      iconTheme: IconThemeData(
        color: Colors.black87,
      ),
      title: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          /*Image.asset(
            'assets/images/qart_logo.png',
            height: 38,
            width: 38,
            fit: BoxFit.scaleDown,
          ),*/
          TextView(title: 'Your Cart', fontColor: Colors.black87, fontSize: 20),
          //TextView(title: addedProducts.length>0?addedProducts[0].qrcode:'', fontColor: Colors.black87, fontSize: 13, margin: EdgeInsets.only(left: 8))
        ],
      ),

    );
  }

  //widget to draw subtotal and buy button card
  Widget _buyButtonCard(){
    if(addedProducts.length>0)
      return  Card(
      elevation: 5,
      margin: EdgeInsets.only(left: 10, right: 10, bottom: 8, top: 8),
      child: Container(
        height: 99,
        padding: EdgeInsets.all(8),
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(4))
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          //mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                TextView(
                    title: 'Subtotal ($_totalQty items): ',
                    fontColor: Colors.black87,
                    fontSize: 16
                ),
                TextView(
                  title: '₹ ${_subtotal}',
                  fontColor: Color.fromRGBO(218, 0, 0, 1),
                  fontSize: 16,
                ),

              ],
            ),
            Container(
              margin: EdgeInsets.only(right: 12, top: 8, bottom: 8),
              child: MaterialButton(
                elevation: 5,
                color: Color.fromRGBO(218, 0, 0, 1),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20.0),
                  //side: BorderSide(color: Colors.red)
                ),
                onPressed: () {

                },
                child: TextView(
                  title: 'Proceed to Buy',
                  fontSize: 16,
                  fontColor: Colors.white,
                ),
              ),
            ),
          ],
        ),
      ),
    );
    else
      return Container();
  }

  //widget to draw product details with quantity increment, decrement & delete button
  Widget _productCardView(){
    return Card(
      elevation: 5,
      child: Container(
        padding: EdgeInsets.all(8),
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(4))
        ),
        child: addedProducts.length>0?ListView.builder(
          shrinkWrap: true,
            itemCount: addedProducts.length??0,
            itemBuilder: (BuildContext ctx, int index){
              return Container(
                height: 90,
                padding: EdgeInsets.only(bottom: 6, left: 6, right: 6),
                child: Row(
                  children: [
                    ClipOval(
                      child: Container(
                        color: Color.fromRGBO(0, 0, 0, 0.1),
                        height: 75,
                        width: 105,
                        child: FadeInImage.assetNetwork(
                          placeholder: 'assets/images/hanger.png',
                          image: addedProducts[index].imageUrl,
                          fit: BoxFit.fitHeight,
                        ),
                      ),
                    ),
                    Expanded(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          TextView(
                              title: '${addedProducts[index].color}',
                              //fontWeight: FontWeight.bold,
                              fontColor: Colors.black,
                              margin: EdgeInsets.only(bottom: 3, left: 8)),
                          TextView(
                              title: 'OP:${addedProducts[index].option}',
                              margin: EdgeInsets.only(left: 8)),
                          TextView(
                              title: 'MAX:₹${addedProducts[index].mrp}',
                              margin: EdgeInsets.only(left: 8))
                        ],
                      ),
                    ),

                    //to draw quantity increment decrement button
                    Container(
                      height: 25,
                      width: 78,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        boxShadow: [BoxShadow(
                          color: Colors.grey, offset: Offset(0.0, 1.0), blurRadius: 4
                        )],
                        border: Border.all(color: Color.fromRGBO(218, 0, 0, 1), width: 1),
                        borderRadius: BorderRadius.all(Radius.circular(2))
                      ),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        //mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                            height: 25,
                            width: 25,
                            child: InkWell(
                              onTap: (){
                                if(addedProducts[index].qty>1){
                                  setState(() {
                                    addedProducts[index].qty--;
                                  });
                                  _updateProductQtyCount(addedProducts[index].option,addedProducts[index].qty);
                                }
                                else
                                  _deleteProduct(addedProducts[index].option);
                                _totalQty--;
                                _getSubtotal();
                              },
                              child:Icon(
                                addedProducts[index].qty>1?Icons.remove:Icons.delete,
                                color: Color.fromRGBO(218, 0, 0, 1), size: 18,
                              ),
                            ),
                          ),
                          Container(
                            width: 1,
                            height: 25,
                            color: Color.fromRGBO(218, 0, 0, 1),
                            //margin: EdgeInsets.only(left: 30),
                          ),
                          Container(
                            height: 25,
                            width: 24,
                            child: Align(
                                alignment: Alignment.center,
                                child: TextView(
                                  title: '${addedProducts[index].qty}',
                                  fontColor: Colors.black,
                                  fontSize: 14
                                )
                            )
                          ),
                          Container(
                            width: 1,
                            height: 25,
                            color: Color.fromRGBO(218, 0, 0, 1),
                            //margin: EdgeInsets.only(left: 30),
                          ),
                          Container(
                            height: 25,
                            width: 25,
                            child: InkWell(
                              onTap: (){
                                if(addedProducts[index].qty<99){
                                  setState(() {
                                    addedProducts[index].qty++;
                                    _totalQty++;
                                  });
                                  _getSubtotal();
                                  _updateProductQtyCount(addedProducts[index].option, addedProducts[index].qty);
                                }
                              },
                              child: Icon(
                                Icons.add,
                                color: Color.fromRGBO(218, 0, 0, 1),
                                size: 18,
                              ),
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              );
            })
        :_emptyCartCard()
      ),
    );
  }

  //widget to draw card when cart is empty
  Widget _emptyCartCard(){
    return Container(
      padding: EdgeInsets.all(16),
      height: 110,
      child: InkWell(
        splashColor: Colors.transparent,
        highlightColor: Colors.transparent,
        onTap: (){
          Navigator.of(context).pop();
        },
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Icon(
              Icons.add_shopping_cart,
              color: Color.fromRGBO(218, 0, 0, 1),
              size: 40,
            ),
            TextView(
                title:'Cart\'s empty!\nTap to add new.',
                textAlign: TextAlign.center,
                fontSize: 16,
                fontColor: Colors.black87
            ),

          ],
        ),
      ),
    );
  }

  //method called when any product gets deleted
  _deleteProduct(String option) async{
    setState(() {
      addedProducts.removeWhere((element) => element.option == option);
    });
    int n = await DatabaseHelper.instance.deleteSpecific(CART_PRODUCTS, option);
    print("Deleted successfully! $n");
  }

   //method to update subtotal every time quantity changes
   _getSubtotal(){
    double subtotal = 0;
    for( var product in addedProducts){
      subtotal += double.parse(product.mrp)*product.qty;
    }
    setState(() {
      _subtotal = subtotal;
    });
  }

  //method to update quantity count in the database
  _updateProductQtyCount(String option, int qty)async{
    int n = await DatabaseHelper.instance.update({ADDED_QTY: qty}, CART_PRODUCTS, option);
    print("Updated successfully! $n rows");
  }

  //method to read cart data from database
  _readFromDB()async{
    List<Map<String, dynamic>> dbData = new List();
    dbData = await DatabaseHelper.instance.queryAll(CART_PRODUCTS);
    print(dbData);
    for(int i=0; i<dbData.length; i++){
      ProductModel model = new ProductModel();
      model.season = dbData[i]['Season'];
      model.brand = dbData[i]['Brand'];
      model.mood = dbData[i]['Mood'];
      model.gender = dbData[i]['Gender'];
      model.option = dbData[i]['Option'];
      model.theme = dbData[i]['Theme'];
      model.category = dbData[i]['Category'];
      model.name = dbData[i]['Name'];
      model.color = dbData[i]['Color'];
      model.mrp = dbData[i]['MRP'];
      model.subCategory = dbData[i]['SubCategory'];
      model.collection = dbData[i]['Collection'];
      model.description = dbData[i]['Description'];
      model.qrcode = dbData[i]['QRCode'];
      model.fabric = dbData[i]['Fabric'];
      model.eans = dbData[i]['EAN'];
      model.finish = dbData[i]['Finish'];
      model.imageUrl = dbData[i]['ImageUrl'];
      model.fit = dbData[i]['Fit'];
      model.tech = dbData[i]['Technology'];
      model.likeability = dbData[i]['Likeability'];
      model.techImageUrl = dbData[i]['Tech_Image_Url'];
      model.qty = dbData[i]['Added_Qty'];
      setState(() {
        addedProducts.add(model);
        _totalQty += model.qty;
      });
    }
    _getSubtotal();
  }

  @override
  void initState(){
    super.initState();
    _readFromDB();
  }
}
