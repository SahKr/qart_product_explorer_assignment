import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:qart_product_explorer/screens/HomeScreen.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {


  //method to start timer for automatically call of navigationPage method
  startTime() async {
    var _duration = new Duration(seconds: 2);
    return new Timer(_duration, navigationPage);
  }

  //method to navigate to the Home Screen
  void navigationPage() {
    Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (BuildContext ctx){
      return HomeScreen();
    }));
  }

  @override
  void initState() {
    super.initState();
    startTime(); // start timer on initialisation
  }

  //build method to display brand logo and name
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.white,
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Image.asset('assets/images/qart_logo.png'),
              Padding(
                padding: EdgeInsets.only(top: 20),
              ),
              Text('QArt Fashion', style: TextStyle(fontSize: 37), ),
            ],
          ),
        ),
      ),
    );
  }

}
