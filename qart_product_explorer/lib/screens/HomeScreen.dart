import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:qart_product_explorer/apis/ApiHandler.dart';
import 'package:qart_product_explorer/commons/CommonFields.dart';
import 'package:qart_product_explorer/database/DatabaseHelper.dart';
import 'package:qart_product_explorer/models/ApiCollectionModel.dart';
import 'package:qart_product_explorer/models/ProductModel.dart';
import 'package:qart_product_explorer/screens/CartScreen.dart';
import 'package:qart_product_explorer/screens/ProductDetailScreen.dart';
import 'package:qart_product_explorer/values/Constant.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  // variable use check if search bar is enabled or not
  bool onSearch = false;

  //variable used to wait until response received from the api
  bool isWaiting = true;

  //variable to store status of internet
  bool isInternetAvail = true;

  //focus node for search text field
  FocusNode _searchFocus = new FocusNode();

  //controller for search text field
  TextEditingController _searchController = new TextEditingController();

  //variable to store product data
  ProductDataModel products = new ProductDataModel();

  //variable to keep product data backup
  ProductDataModel allProducts = new ProductDataModel();

  @override
  Widget build(BuildContext context) {
    //FocusScopeNode currentFocus = FocusScope.of(context);
    return Scaffold(
      appBar: _appBar(),
      body: Stack(
        children: [
          Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              //crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Row(
                  children: [
                    Expanded(
                      child: Container(
                        margin: EdgeInsets.only(left: 12, right: 8),
                        height: 1,
                        color: Colors.black,
                      ),
                    ),
                    TextView(
                        title: (onSearch ? 'Searched ' : 'All ') + 'Products',
                        fontColor: Colors.black,
                        fontSize: 16,
                        //fontWeight: FontWeight.bold,
                        margin: EdgeInsets.only(top: 6, bottom: 2)),
                    Expanded(
                      child: Container(
                        margin: EdgeInsets.only(left: 8, right: 12),
                        height: 1,
                        color: Colors.black,
                      ),
                    ),
                  ],
                ),
                products.products.length>0?Container(
                  padding:EdgeInsets.only(left: 8, right: 8, top: 4, bottom: 4),
                  margin: EdgeInsets.only( bottom: 4),
                  decoration: BoxDecoration(
                      color: Color.fromRGBO(218, 0, 0, 1),
                      borderRadius: BorderRadius.all(Radius.circular(25))
                  ),
                  child: TextView(
                      title: 'Product Count: ${products.productCount}',
                      fontColor: Colors.white,
                      textAlign: TextAlign.center
                  ),
                ):Container(),
                Expanded(
                  child: products.products.length>0?ListView.builder(   //to display products list onto the screen
                      itemCount: products.productCount ?? 0,
                      itemBuilder: (BuildContext ctx, int index) {
                        return cardView(products.products[index]);
                      }):!isInternetAvail?
                  Center(
                    child: Wrap(
                      direction: Axis.vertical,
                      spacing: 12,
                      crossAxisAlignment: WrapCrossAlignment.center,
                      children: [
                        Icon(Icons.warning, size: 40,),
                        TextView(
                          title: 'No data available!\nTry refreshing!',
                          fontColor: Colors.black87,
                          fontSize: 16,
                          textAlign: TextAlign.center
                        ),
                      ],
                    ),
                  ):Container(),
                )
              ],
            ),
          ),
          isWaiting ? _loadingCard() : Container()
        ],
      ),
    );
  }

  //Widget to draw appBar
  Widget _appBar(){
    return AppBar(
      backgroundColor: Colors.white,
      title: !onSearch //condition to replace appbar with search textField and vice versa
          ? Container(
        color: Colors.white,
        child: Row(
          //mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            TextView(
              title: 'Product Explorer',
              fontSize: 21,
              fontColor: Colors.black87,
              //fontWeight: FontWeight.bold
            ),
            Expanded(
              child: Container(),
            ),
            iconContainer(Icons.shopping_cart, EdgeInsets.only(right: 8), () {
              print('cart button pressed!');
              Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext ctx){
                return CartScreen();
              }));

            }, 26, Color.fromRGBO(218, 0, 0, 1)),
            iconContainer(Icons.refresh, EdgeInsets.only(right: 8), () {
              print('refresh pressed!');
              setState(() {
                isWaiting = true;
              });
              _callApi();
            }, 26, Color.fromRGBO(218, 0, 0, 1)),
            iconContainer(Icons.search, null, () {
              print('search pressed!');
              setState(() {
                onSearch = true;
                products = new ProductDataModel();
                //currentFocus.requestFocus(_searchFocus);
              });
            }, 27, Color.fromRGBO(218, 0, 0, 1)),
          ],
        ),
      )
          : Container(
        child: TextField(
            maxLines: 1,
            style: TextStyle(fontSize: 20),
            focusNode: _searchFocus,
            controller: _searchController,
            onChanged: (value) {
              setState(() {
                _searchController..text;
              });
              _onSearch(_searchController.text);
            },
            decoration: new InputDecoration(
                prefixIcon: iconContainer(
                    Icons.arrow_back, EdgeInsets.only(right: 12), () {
                  print('back pressed!');
                  setState(() {
                    onSearch = false;
                    products = allProducts;
                    _searchController.text = '';
                  });
                }, 27, Colors.black54),
                suffixIcon: iconContainer(
                    Icons.clear, EdgeInsets.only(left: 12), () {
                  print('cross pressed!');
                  setState(() {
                    //onSearch = true;
                    _searchController.text = '';
                    //searchedProducts = new ProductDataModel();
                    products = new ProductDataModel();
                  });
                }, 27, _searchController.text.length>0?Color.fromRGBO(218, 0, 0, 1):Colors.white),
                border: InputBorder.none,
                focusedBorder: InputBorder.none,
                enabledBorder: InputBorder.none,
                errorBorder: InputBorder.none,
                disabledBorder: InputBorder.none,
                hintStyle: TextStyle(fontSize: 20),
                contentPadding: EdgeInsets.only(
                    left: 15, bottom: 15, top: 15, right: 15),
                hintText: 'Search')),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    _callApi();
  }

  //loading card displaying while waiting for api response
  Widget _loadingCard() {
    return Center(
      child: Container(
        padding: EdgeInsets.all(10),
        height: 90,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(10)),
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                  color: Colors.grey, offset: Offset(0.0, 1.0), blurRadius: 5)
            ]),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            TextView(
                title: "Fetching data...",
                fontSize: 17,
                fontColor: Colors.black87
                //margin: EdgeInsets.only( bottom: 8)
                ),
            Padding(
              padding: const EdgeInsets.only(
                top: 12,
              ),
              child: CircularProgressIndicator(
                valueColor: new AlwaysStoppedAnimation<Color>(Color.fromRGBO(218, 0, 0, 1)),
              ),
            )
          ],
        ),
      ),
    );
  }

  //method to call api
  _callApi() async {
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        print('connected');
        setState(() {
          isInternetAvail = true;
        });
      }
    } on SocketException catch (_) {
      print('not connected');
      showDialogMsg(context, 'No internet!\nPlease check your internet and refresh!');
      setState(() {
        isWaiting = false;
        isInternetAvail = false;
      });
      return ;
    }
    ApiCollectionModel collectionResponse =
        await ApiHandler().hitApiCollection();
    if (collectionResponse != null) {
      //List<Map<String, dynamic>> values = await DatabaseHelper.instance.queryAll();
      //print(values[0]['_id']);
      ProductDataModel productsResponse = await ApiHandler()
          .hitProductApi(collectionResponse.apiUrl, collectionResponse.method);
      if (productsResponse != null) {
        setState(() {
          products = productsResponse;
          allProducts = productsResponse;

        });
        var size = await DatabaseHelper.instance.queryAll(STORE_PRODUCTS);

        /*Checking if the received data is updated or is it the first time call
        to the api i.e database is empty.*/
        if (productsResponse.lastUpdatedToken == 1 ||
            size == null ||
            size.length == 0) {

          //if received data is updated then replacing new data with previous data
          if (size != null && size.length > 0) DatabaseHelper.instance.delete(STORE_PRODUCTS);
          for (int i = 0; i < productsResponse.productCount; i++) {

            await DatabaseHelper.instance.insert({
              SEASON: productsResponse.products[i].season,
              BRAND: productsResponse.products[i].brand,
              MOOD: productsResponse.products[i].mood,
              GENDER: productsResponse.products[i].gender,
              THEME: productsResponse.products[i].theme,
              CATEGORY: productsResponse.products[i].category,
              NAME: productsResponse.products[i].name,
              COLOR: productsResponse.products[i].color,
              OPTION: productsResponse.products[i].option,
              MRP: productsResponse.products[i].mrp,
              SUBCATEGORY: productsResponse.products[i].subCategory,
              COLLECTION: productsResponse.products[i].collection,
              DESCRIPTION: productsResponse.products[i].description,
              QRCODE: productsResponse.products[i].qrcode,
              FABRIC: productsResponse.products[i].fabric,
              EANS: productsResponse.products[i].eans,
              FINISH: productsResponse.products[i].finish,
              IMAGEURL: productsResponse.products[i].imageUrl,
              FIT:productsResponse.products[i].fit,
              TECHNOLOGY:productsResponse.products[i].tech,
              LIKEABILTY:productsResponse.products[i].likeability,
              TECH_IMAGE_URL:productsResponse.products[i].techImageUrl,
              OFF_MONTHS:productsResponse.products[i].offerMonths.toString()
            }, STORE_PRODUCTS);
          }
          print("Done!");
        }
        setState(() {
          isWaiting = false;
        });
      } else {
        setState(() {
          isWaiting = false;
        });
        showDialogMsg(context, 'Something went wrong!\nPlease refresh!');
      }
    } else {
      setState(() {
        isWaiting = false;
      });
      showDialogMsg(context, 'Something went wrong!\nPlease refresh!');
    }
  }

  //widget for displaying product card
  Widget cardView(ProductModel product) {
    return Card(
        elevation: 5,
        margin: EdgeInsets.only(left: 10, right: 10, bottom: 4),
        child: InkWell(
          onTap: () {
            Navigator.of(context)
                .push(MaterialPageRoute(builder: (BuildContext ctx) {
              return ProductDetailScreen(product);
            }));
          },
          splashColor: Color.fromRGBO(255, 0, 0, 0.2),
          highlightColor: Colors.transparent,
          child: ExpansionTile(
              initiallyExpanded: true,
              title: TextView(
                  title: '${product.option}',
                  fontSize: 17,
                  fontColor: Colors.black87

              ),
              children:[
                Container(
                  height: 90,
                  padding: EdgeInsets.only(bottom: 6, left: 6, right: 6),
                  child: Row(
                    children: [
                      ClipOval(
                        child: Container(
                          color: Color.fromRGBO(0, 0, 0, 0.1),
                          height: 75,
                          width: 105,
                          child: FadeInImage.assetNetwork(
                            placeholder: 'assets/images/hanger.png',
                            image: product.imageUrl,
                            fit: BoxFit.fitHeight,
                          ),
                        ),
                      ),
                      Expanded(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            TextView(
                                title: '${product.color}',
                                //fontWeight: FontWeight.bold,
                                fontColor: Colors.black,
                                margin: EdgeInsets.only(bottom: 3, left: 8)),
                            TextView(
                                title: 'OP:${product.qrcode}',
                                margin: EdgeInsets.only(left: 8)),
                            TextView(
                                title: 'MAX:₹${product.mrp}',
                                margin: EdgeInsets.only(left: 8))
                          ],
                        ),
                      ),
                      Container(
                        width: 72,
                        child: Wrap(
                          direction: Axis.horizontal,
                          children: product.eans.split(' ')
                              .map((e) =>
                              TextView(title: '$e, ', fontColor: Colors.black))
                              .toList(),
                        ),
                      )
                    ],
                  ),
                ),
              ]
          ),
        ));
  }

  //method for searching products , called when search text changed
  //replacing product with searched product
  _onSearch(String str) {
    if (str == '')
      setState(() {
        products = new ProductDataModel();
      });
    else {
      ProductDataModel searchProducts = new ProductDataModel();
      for (ProductModel product in allProducts.products) {
        if (product.option.toLowerCase().contains(str.toLowerCase()) ||
            product.qrcode.toLowerCase().contains(str.toLowerCase())) {
          searchProducts.products.add(product);
          searchProducts.productCount++;
        }
      }
      setState(() {
        products = searchProducts;
      });
    }
  }
}
