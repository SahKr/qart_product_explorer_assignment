import 'dart:convert';

import 'package:qart_product_explorer/models/ApiCollectionModel.dart';

class ApiCollectionMapper{

  ApiCollectionModel apiMapper(String jsonString){
    Map<String, dynamic> jsonMap = json.decode(jsonString);
    print(jsonMap);
    ApiCollectionModel model = new ApiCollectionModel();
    var item = jsonMap["item"][0];
    model.apiName  = item["name"]??'';
    var request = item["request"];
    model.method = request["method"]??'';
    var url = request["url"];
    model.apiUrl = url["raw"]??'';
    model.protocol = url["protocol"]??'';

    return model;
  }
}