import 'dart:convert';

import 'package:qart_product_explorer/models/ProductModel.dart';

class ProductDataMapper{

  ProductDataModel apiMapper(String jsonString){
    Map<String, dynamic> jsonMap = json.decode(jsonString);
    print(jsonMap);
    ProductDataModel dataModel = new ProductDataModel();
    dataModel.productCount = jsonMap['ProductCount'];
    for(var data in jsonMap['Products']){
      String eans = '';
      Map eanMap = data["EAN"];
      List eanKeyList = eanMap.keys.toList();
      for(String ean in eanKeyList){
        eans += ean+' ';
      }
      ProductModel model = new ProductModel();
      model.season = data['Season']??'';
      model.brand = data['Brand']??'';
      model.mood = data['Mood']??'';
      model.gender = data['Gender']??'';
      model.theme = data['Theme']??'';
      model.category = data['Category']??'';
      model.name = data['Name']??'';
      model.color = data['Color']??'';
      model.option = data['Option']??'';
      model.mrp = data['MRP'].toString()??'';
      model.subCategory = data['SubCategory']??'';
      model.collection = data['Collection']??'';
      model.description = data['Description']??'';
      model.qrcode = data['QRCode']??'';
      model.offerMonths = data['OfferMonths']??'';
      model.fabric = data['Fabric']??'';
      model.finish = data['Finish']??'';
      model.imageUrl = data['ImageUrl']??'';
      model.fit = data['Fit']??'';
      model.tech = data['Technology']??'';
      model.likeability = data['Likeabilty'].toString()??'';
      model.techImageUrl = data['TechnologyImageUrl'];
      model.eans = eans.trim();
      dataModel.products.add(model);
    }

    return dataModel;
  }
}