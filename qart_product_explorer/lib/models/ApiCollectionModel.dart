class ApiCollectionModel{
  String _apiName = '';
  String _apiUrl = '';
  String _protocol = '';
  String _queryKey = '';
  String _queryValue = '';
  String _method = '';

  String get method => _method;

  set method(String value) {
    _method = value;
  }

  String get apiName => _apiName;

  set apiName(String value) {
    _apiName = value;
  }

  String get apiUrl => _apiUrl;

  String get queryValue => _queryValue;

  set queryValue(String value) {
    _queryValue = value;
  }

  String get queryKey => _queryKey;

  set queryKey(String value) {
    _queryKey = value;
  }

  String get protocol => _protocol;

  set protocol(String value) {
    _protocol = value;
  }

  set apiUrl(String value) {
    _apiUrl = value;
  }
}