class ProductModel{
  String _season = '';

  String get season => _season;

  set season(String value) {
    _season = value;
  }

  String _brand = '';
  int _index = 0;

  int get index => _index;

  set index(int value) {
    _index = value;
  }

  String _mood = '';
  String _gender = '';
  String _theme = '';
  String _category = '';
  String _name = '';
  String _color = '';
  String _option = '';
  String _mrp = '';
  String _subCategory = '';
  String _collection = '';
  String _description = '';
  String _qrcode = '';
  String _fabric = '';
  String _imageUrl = '';
  String _finish = '';
  String _fit = '';
  String _likeability = '';
  String _techImageUrl = '';

  String get techImageUrl => _techImageUrl;

  set techImageUrl(String value) {
    _techImageUrl = value;
  }

  int _qty = 0;

  int get qty => _qty;

  set qty(int value) {
    _qty = value;
  }

  String get likeability => _likeability;

  set likeability(String value) {
    _likeability = value;
  }

  String get fit => _fit;

  set fit(String value) {
    _fit = value;
  }

  String _tech = '';


  String get finish => _finish;

  set finish(String value) {
    _finish = value;
  }

  List _offerMonths = [];

  List get offerMonths => _offerMonths;

  String _eans = '';

  String get eans => _eans;

  set eans(String value) {
    _eans = value;
  }

  set offerMonths(List value) {
    _offerMonths = value;
  }

  String get brand => _brand;

  set brand(String value) {
    _brand = value;
  }

  String get mood => _mood;

  set mood(String value) {
    _mood = value;
  }

  String get gender => _gender;

  set gender(String value) {
    _gender = value;
  }

  String get theme => _theme;

  set theme(String value) {
    _theme = value;
  }

  String get category => _category;

  set category(String value) {
    _category = value;
  }

  String get name => _name;

  set name(String value) {
    _name = value;
  }

  String get color => _color;

  set color(String value) {
    _color = value;
  }

  String get option => _option;

  set option(String value) {
    _option = value;
  }

  String get mrp => _mrp;

  set mrp(String value) {
    _mrp = value;
  }

  String get subCategory => _subCategory;

  set subCategory(String value) {
    _subCategory = value;
  }

  String get collection => _collection;

  set collection(String value) {
    _collection = value;
  }

  String get description => _description;

  set description(String value) {
    _description = value;
  }

  String get qrcode => _qrcode;

  set qrcode(String value) {
    _qrcode = value;
  }

  String get fabric => _fabric;

  set fabric(String value) {
    _fabric = value;
  }

  String get imageUrl => _imageUrl;

  set imageUrl(String value) {
    _imageUrl = value;
  }

  String get tech => _tech;

  set tech(String value) {
    _tech = value;
  }
}

class ProductDataModel{
  List<ProductModel> _products = new List<ProductModel>();

  List<ProductModel> get products => _products;

  set products(List<ProductModel> value) {
    _products = value;
  }

  int _productCount = 0;

  int get productCount => _productCount;

  set productCount(int value) {
    _productCount = value;
  }

  int _lastUpdatedToken = 0;

  int get lastUpdatedToken => _lastUpdatedToken;

  set lastUpdatedToken(int value) {
    _lastUpdatedToken = value;
  }
}