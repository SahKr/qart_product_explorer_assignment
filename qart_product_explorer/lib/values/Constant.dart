//api urls
//const GET_PRODUCTS_URL = 'https://debug.qart.fashion/api/product/GetProductsWithSizes?retailerCode=40984';
const API_COLLECTION_URL = 'https://www.getpostman.com/collections/689b0bc8d604145661f9';

//table names
const STORE_PRODUCTS = 'productData';
const CART_PRODUCTS = 'productsInCart';

//database table column names
const ID = '_id';
const SEASON = 'Season';
const BRAND = 'Brand';
const MOOD = 'Mood';
const GENDER = 'Gender';
const THEME = 'Theme';
const CATEGORY = 'Category';
const NAME = 'Name';
const COLOR = 'Color';
const OPTION = 'Option';
const MRP = 'MRP';
const SUBCATEGORY = 'SubCategory';
const COLLECTION = 'Collection';
const DESCRIPTION = 'Description';
const QRCODE = 'QRCode';
const FABRIC = 'Fabric';
const OFF_MONTHS = 'Off_Months';
const EANS = 'EAN';
const FINISH = 'Finish';
const IMAGEURL = 'ImageUrl';
const FIT = 'Fit';
const TECHNOLOGY = 'Technology';
const LIKEABILTY = 'Likeabilty';
const ADDED_TO_CART = 'Added_to_cart';
const ADDED_QTY = 'Added_Qty';
const TECH_IMAGE_URL = 'Tech_Image_Url';

